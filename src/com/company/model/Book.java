package com.company.model;

/**
 * Created by SourceIt on 02.12.2017.
 */
public class Book extends Literature {
    String author;

    public Book(int year, String publisher, String author) {
        super(year, publisher);
        this.author = author;
    }


    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                "} " + super.toString();
    }
}
