package com.company.model;

/**
 * Created by SourceIt on 02.12.2017.
 */


public final class Generator {

    private Generator() {
    }

    public static Literature[] generateLiteratures() {
        Literature[] literatures = new Literature[9];

        literatures[0] = new Book(1995, "Publisher 1", "Author 1");
        literatures[1] = new Book(1958, "Publisher 3", "Author 3");
        literatures[2] = new Book(1999, "Publisher 2", "Author 2");
        literatures[3] = new Magazine(1995, "Publisher 4", 2);
        literatures[4] = new Magazine(1958, "Publisher 5", 3);
        literatures[5] = new Magazine(1999, "Publisher 6", 4);
        literatures[6] = new Literature(1985, "Publisher 7");
        literatures[7] = new Literature(1978, "Publisher 8");
        literatures[8] = new Literature(1989, "Publisher 9");


        return literatures;
    }
}

