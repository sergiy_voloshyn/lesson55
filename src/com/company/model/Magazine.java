package com.company.model;

/**
 * Created by SourceIt on 02.12.2017.
 */
public class Magazine extends Literature {
    int articles;

    public Magazine(int year, String publisher, int articles) {
        super(year, publisher);
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "articles=" + articles +
                "} " + super.toString();
    }
}
