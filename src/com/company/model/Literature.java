package com.company.model;

/**
 * Created by SourceIt on 02.12.2017.
 */
public class Literature {

    int year;
    String publisher;

    public Literature(int year, String publisher) {
        this.year = year;
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Literature{" +
                "year=" + year +
                ", publisher='" + publisher + '\'' +
                '}';
    }

}
